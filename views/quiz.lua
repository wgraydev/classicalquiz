--local storyboard = require "storyboard"
local scene      = storyboard.newScene()
local answer, wrongAns, composerButton, composerButton1
local score = 0
local scoreText
local guessResultText
local song
local composerImage
local isAnswerButton1, isAnswerButton2
local currentComposer
display.setStatusBar(display.HiddenStatusBar)

--generates a random integer that is in the range from 1 to the total number of composers
function randomSongNum()
	return math.random(#songlist)
end
function randComposerNum()
	return math.random(#composerlist)
end
function getSongPath(id)
	return songlist[id].path
end
function getComposer(id)
	return songlist[id].composer
end
function showComposerImage( composerName )
	local imagePath = "images/" .. composerName .. ".jpg"

	composerImage = display.newImageRect( imagePath, 200, 200 )
	composerImage:setReferencePoint( display.CenterReferencePoint )
	composerImage.x = display.contentWidth/2
	composerImage.y = display.contentHeight/3
end
function destroyComposerImage()
	-- hide the object
	--composerImage.isVisible = false
	-- remove it
	if composerImage ~= nil then
		-- if removeSelf is called on composer Image then app crashes
		composerImage:removeSelf()
	end
	composerImage = nil
end
function getWrongComposer(composer)
	local rand = randComposerNum()
	while(composer == composerlist[rand]) do
		rand = randComposerNum()
	end
	-- return a different composer than composer
	return composerlist[rand]
end
function MenuButtonPress()
	print("MenuButtonPress")
	destroyQuestion()
	storyboard.gotoScene("views.menu")     
end
function Button1Press()
	print("Button1Press")
	if isAnswerButton1 then 
		print("you got the answer correct")
		score = score + 10
	else
		print("incorrect")
		score = score - 10
	end
	updateScore()
	destroyQuestion()

	showComposerImage(currentComposer)
	timer.performWithDelay( 3000, destroyComposerImage)
	
	setResultText(isAnswerButton1)
	timer.performWithDelay( 3000, clearResultText )

	createQuestion()
end
function Button2Press()
	print("Button2Press was pressed")
	if isAnswerButton2 then 
		print("you got the answer correct")
		score = score + 10
	else
		print("incorrect")
		score = score - 10
	end
	updateScore()
	destroyQuestion()

	showComposerImage(currentComposer)
	timer.performWithDelay( 3000, destroyComposerImage)
	
	setResultText(isAnswerButton2)
	timer.performWithDelay( 3000, clearResultText )

	createQuestion()
end
function clearResultText()
	guessResultText.text = ""
end
function setResultText(isCorrect)
	if isCorrect then
		guessResultText.text = "Correct"
	else
		guessResultText.text = "Incorrect"
	end
end
function updateScore()
	scoreText.text = "Score: " .. score
end
function saveHighScore(  )

	db = sqlite3.open_memory()
	db:exec[[
  CREATE TABLE IF NOT EXISTS high_score (id INTEGER PRIMARY KEY, points INTEGER); 
  INSERT INTO high_score VALUES (1, 100)]]

	print("database and table created")
	print("save High score called")
	--db:exec[[ SELECT points from high_score WHERE id = '1' ]]
	local currentHighScore
	-- pull out the current high score
	for row in db:nrows("SELECT * FROM high_score") do
  		print("Score pulled from db: " .. row.points )
  		currentHighScore = row.points
  	end 

  	--if the score is greater than the current high score then store the score
	if score > currentHighScore then
		local q = [[UPDATE high_score SET points = ']] .. score .. [[' WHERE id = 1 ]]
		db:exec(q)
	end

	for row in db:nrows("SELECT * FROM high_score") do
  		print("Score pulled from db: " .. row.points )
  	end	
end
--generate quiz
function createQuestion()
	--play song
	local rand = randomSongNum()
	local songPath = songlist[rand].path
	song = audio.loadStream(songPath)
	local songChannel = audio.play(song)
	currentComposer = songlist[rand].composer
	print("currentComposer" .. currentComposer)

	-- generate answer buttons and texts
	if math.random(2) == 2 then

		--button 1
		answer.text = currentComposer --display.newText( currentComposer, 0, 0, native.systemFontBold, 35 )
		answer:setTextColor( 255 )
		answer:setReferencePoint( display.CenterReferencePoint )
		answer.x = display.contentWidth/2; answer.y = display.contentHeight*5/9

		isAnswerButton1 = true 

		wrongAns.text = getWrongComposer(currentComposer) --display.newText( getWrongComposer(currentComposer), 0, 0, native.systemFontBold, 35 )
		wrongAns:setTextColor( 255 )
		wrongAns:setReferencePoint( display.CenterReferencePoint )
		wrongAns.x = display.contentWidth/2; wrongAns.y = display.contentHeight*6/9

		isAnswerButton2 = false 		
	else
		--button 1
		wrongAns.text = getWrongComposer(currentComposer) -- display.newText( getWrongComposer(currentComposer), 0, 0, native.systemFontBold, 35 )
		wrongAns:setTextColor( 255 )
		wrongAns:setReferencePoint( display.CenterReferencePoint )
		wrongAns.x = display.contentWidth/2; wrongAns.y = display.contentHeight*5/9

		isAnswerButton1 = false

		answer.text = currentComposer -- display.newText( currentComposer, 0, 0, native.systemFontBold, 35 )
		answer:setTextColor( 255 )
		answer:setReferencePoint( display.CenterReferencePoint )
		answer.x = display.contentWidth/2; answer.y = display.contentHeight*6/9

		isAnswerButton2 = true	
	end
end
function destroyQuestion()
	audio.stop()
	audio.dispose( song )
	song = nil
end
function scene:createScene( event )
	local screenGroup = scene.view


	local background  = display.newRect(0,0, display.contentWidth, display.contentHeight)
	background:setFillColor(232,100,27)
	screenGroup:insert(background)
	
	composerButton    = display.newImage("buttons/answerButton.png", 0, 0)
	composerButton.x  = display.contentWidth/2; composerButton.y = display.contentHeight*5/9
	screenGroup:insert(composerButton)
	
	composerButton1   = display.newImage("buttons/answerButton.png", 0, 0)
	composerButton1.x = display.contentWidth/2; composerButton1.y = display.contentHeight*6/9
	screenGroup:insert(composerButton1)
	
	menuButton        = display.newImage( "buttons/menu.png", 0, 0 )
	menuButton.x      = display.contentWidth/4; menuButton.y = display.contentHeight*8/9
	screenGroup:insert(menuButton)

	
	answer = display.newText( ". ", 0, 0, native.systemFontBold, 35 )
	wrongAns = display.newText( ".  ", 0, 0, native.systemFontBold, 35 )
	--createQuestion()

	screenGroup:insert( answer )
	screenGroup:insert( wrongAns )

	composerButton:addEventListener( "tap" , Button1Press)
	composerButton1:addEventListener("tap", Button2Press)
	menuButton:addEventListener("tap", MenuButtonPress)

	-- Current Score
	scoreText = display.newText( score, 0, 0, native.systemFont, 32 )
	scoreText:setTextColor( 255 )
	scoreText:setReferencePoint( display.CenterReferencePoint )
	scoreText.x, scoreText.y = display.contentWidth * 0.5, display.contentHeight/10 * 0.5
	screenGroup:insert( scoreText )

	-- Result Text
	guessResultText = display.newText( "", 0, 0, native.systemFont, 35 )
	guessResultText:setTextColor( 255 )
	guessResultText:setReferencePoint( display.CenterReferencePoint )
	guessResultText.x, guessResultText.y = display.contentWidth * 0.5, display.contentHeight/3 * 0.5
	screenGroup:insert( guessResultText )

	-- --image
	-- local image = display.newImageRect( "image.png", 100, 100 )
	-- image:setReferencePoint( display.CenterReferencePoint )
	-- image.x = display.contentCenterX
	-- image.y = display.contentCenterY


	
	print( "\n1: createScene event")
	--TESTING DATABASE ACCESS
		saveHighScore()
	--
end
function scene:enterScene(event)
	createQuestion()
end

scene:addEventListener( "createScene" )
scene:addEventListener( "enterScene")

return scene