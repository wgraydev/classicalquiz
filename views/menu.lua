--------------------------------------
-- 	menu.lua
--------------------------------------
--local storyboard = require "storyboard" 
local scene = storyboard.newScene()

-- hide status bar
display.setStatusBar( display.HiddenStatusBar )



local takeQuizButton
local aboutButton
local biographyButton
local helpButton
local donateButton
--local background


local function quizButtonTouch(event)
     print("take quiz button pressed"); 
     storyboard.gotoScene("views.quiz")     
    
end

local function aboutButtonTouch(event)
     print("about button pressed");
     storyboard.gotoScene("views.about")     
end

local function biographyButtonTouch(event)
     print("biography button pressed");     
end

local function helpButtonTouch(event)
     print("help button pressed");     
end

local function donateButtonTouch(event)
     print("donate quiz button pressed");     
end

-- Called when the scene's view does not exist:
function scene:createScene( event )

	local group = self.view
	local background = display.newRect(0,0, display.contentWidth, display.contentHeight)
	background:setFillColor(232,100,27)
	group:insert(background)

	logo = display.newImage("images/logo.png", 0, 0)
	logo.x = display.contentWidth/2; logo.y = display.contentHeight/5
	group:insert(logo)

	takeQuizButton = display.newImage("buttons/takeQuiz.png", 0, 0)
	takeQuizButton.x = display.contentWidth/2; takeQuizButton.y = display.contentHeight*4/9
	group:insert(takeQuizButton)

	aboutButton = display.newImage("buttons/about.png", 0, 0)
	aboutButton.x = display.contentWidth/2; aboutButton.y = display.contentHeight*5/9
	group:insert(aboutButton)

	biographyButton = display.newImage("buttons/biography.png", 0, 0)
	biographyButton.x = display.contentWidth/2; biographyButton.y = display.contentHeight*6/9
	group:insert(biographyButton)

	helpButton = display.newImage("buttons/help.png", 0, 0)
	helpButton.x = display.contentWidth/2; helpButton.y = display.contentHeight*7/9
	group:insert(helpButton)

	donateButton = display.newImage("buttons/donate.png", 0, 0)
	donateButton.x = display.contentWidth/2; donateButton.y = display.contentHeight*8/9
	group:insert(donateButton)

end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	-- event listeners
	takeQuizButton:addEventListener("tap",quizButtonTouch)
	aboutButton:addEventListener("tap",aboutButtonTouch)
	biographyButton:addEventListener("tap",biographyButtonTouch)
	helpButton:addEventListener("tap",helpButtonTouch)
	donateButton:addEventListener("tap",donateButtonTouch)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	takeQuizButton:removeEventListener("tap",quizButtonTouch)
	aboutButton:removeEventListener("tap",aboutButtonTouch)
	biographyButton:removeEventListener("tap",biographyButtonTouch)
	helpButton:removeEventListener("tap",helpButtonTouch)
	donateButton:removeEventListener("tap",donateButtonTouch)
end

-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	--audio.dispose() 
end

scene:addEventListener( "createScene", scene )
scene:addEventListener( "enterScene", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "destroyScene", scene )

return scene