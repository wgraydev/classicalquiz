--------------------------------------
-- 	main.lua
--	created by: Landon Gray
--------------------------------------
--Global Scope
storyboard   = require "storyboard"
songlist     = require "songlist"
composerlist = require "composerlist"
require "sqlite3"



local scene = storyboard.newScene()

--require("views.menu")

storyboard.gotoScene( "views.menu", "fade", 400 )
